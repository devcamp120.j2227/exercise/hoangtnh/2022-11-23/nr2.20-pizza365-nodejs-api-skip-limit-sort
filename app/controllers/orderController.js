//Import thư viện Mongoose
const mongoose = require("mongoose");
//Import order model
const orderModel = require("../model/orderModel");
// Import user model
const userModel = require ("../model/userModel");

//function create new order
const createOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;
    console.log(body)

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    if (!body.orderCode) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order code không hợp lệ"
        })
    }
    if (!body.pizzaSize) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Pizza size không hợp lệ"
        })
    }
    if (!body.pizzaType) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Pizza type không hợp lệ"
        })
    }
    if (!body.status) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Status không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status
    }

    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Thêm ID của order mới vào mảng orders của user đã chọn
        userModel.findByIdAndUpdate(userId, {
            $push: {
                orders: data._id
            }
        }, (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Order Successfully",
                data: data
            })
        })
    })
}

//function get all orders
const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all orders successfully",
            data: data
        })
    })
}

//function get order by id
const getOrderById = (request,response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail order successfully",
            data: data
        })
    })
}
//Extra
//get all order by user id
const getAllDetailOrderByUserId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all orders of user successfully",
                data: data
            })
        })
}
//function update order by id
const updateOrderById = (request, response) =>{
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }
    if (!body.orderCode) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order code không hợp lệ"
        })
    }
    if (!body.pizzaSize) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Pizza size không hợp lệ"
        })
    }
    if (!body.pizzaType) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Pizza type không hợp lệ"
        })
    }
    if (!body.status) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Status không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateOrder = {}

    if (body.orderCode !== undefined) {
        updateOrder.orderCode = body.orderCode
    }

    if (body.pizzaSize !== undefined) {
        updateOrder.pizzaSize = body.pizzaSize
    }
    if (body.pizzaType !== undefined) {
        updateOrder.pizzaType = body.pizzaType
    }
    if (body.status !== undefined) {
        updateOrder.status = body.status
    }

    orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update order successfully",
            data: data
        })
    })
}
//function delete order by id
const deleteOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
      
        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        userModel.findByIdAndUpdate(userId, {
            $pull: { orders: orderId }
        }, (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
                
            }
            return response.status(200).json({
                status: "Delete order successfully"
            })
        })
        
    })
}
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    getAllDetailOrderByUserId,
    updateOrderById,
    deleteOrderById
}