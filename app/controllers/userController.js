//Import thư viện Mongoose
const mongoose = require("mongoose");
//Import user model
const userModel = require("../model/userModel");

//function create user
const createUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    console.log(body);
    //B2: validate dữ liệu
    if(!body.fullName){
        return response.status(400).json({
            status: "Bad Request",
            message: "Tên không hợp lệ"
        })
    }
    if(!body.email){
        return response.status(400).json({
            status: "Bad Request",
            message: "Email không hợp lệ"
        })
    }
    if(!body.address){
        return response.status(400).json({
            status: "Bad Request",
            message: "Địa chỉ không hợp lệ"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status: "Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    }

    userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create new user successfully",
            data: data
        })
    })
}

//function get all users
const getAllUsers = (request, response) => {
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all users successfully",
            data: data
        })
    })
}
//function get user by id
const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail user successfully",
            data: data
        })
    })
}
//function update user ById
const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }

    if(body.fullName !== undefined && body.fullName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Tên không hợp lệ"
        })
    }

    if(body.email !== undefined && body.email.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Email không hợp lệ"
        })
    }
    if(body.address !== undefined && body.address.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Địa chỉ không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim()==="") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateUser = {}

    if(body.fullName !== undefined) {
        updateUser.fullName = body.fullName
    }
    if(body.email !== undefined) {
        updateUser.email = body.email
    }
    if(body.address !== undefined) {
        updateUser.address = body.address
    }
    if(body.phone !== undefined) {
        updateUser.phone = body.phone
    }
    
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update user successfully",
            data: data
        })
    })
}

//function delete User ById
const deleteUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete user successfully"
        })
    })
}
//function get all user limit
const getAllUserLimit =  (request, response) =>{
    const limitNumber = request.query.limitNumber;
    //nếu sử dụng limit tại url /api/limit-users?limitNumber=(number)
    if(limitNumber){
            userModel.find()
            .limit(limitNumber)
            .exec((error, data)  => {
                if(error) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                } 
                else {
                    return response.status(200).json({
                        status: "Success: Get limited users success",
                        data: data
                    })
                }
            })
        }
        //trường hợp không sử dụng limit number
        else {
            userModel.find((error, data) => {
                if(error) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                } 
                    return response.status(200).json({
                        status: "Success: Get All users success",
                        data: data
                    })
                
            })
        }
}
//function get all user with skip method using request.query
const getAllUserSkip = (request, response) => {
    let skipNumber = request.query.skipNumber;
    if(skipNumber) {
        userModel.find()
            .skip(skipNumber)
            .exec((error, data)  => {
                if(error) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                } 
                else {
                    return response.status(200).json({
                        status: "Success: Get skipped users success",
                        data: data
                    })
                }
            })
            
    }
    else {
        userModel.find((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } 
                return response.status(200).json({
                    status: "Success: Get All users success",
                    data: data
                })
            
        })
    }
}

//function sort user by full name alphabet
const sortUserByABC = (request, response) => {
    userModel.find()
    .sort({fullName:"asc"})
    .exec((error, data)  => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } 
        else {
            return response.status(200).json({
                status: "Success: Get users by alphabet success",
                data: data
            })
        }
    })

}

//function skip + limit get user
const getAllUserSkipLimit = (request, response) => {
    const skipNumber = request.query.skipNumber;
    if(skipNumber) {
        userModel.find()
                .skip(skipNumber)
                .limit(1) //trả limit phần tử đầu tiên tính từ vị trí skip
                .exec((error, data)  => {
                    if(error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } 
                    else {
                        return response.status(200).json({
                            status: "Success: Get skip limit users success",
                            data: data
                        })
                    }
                })
        }
        else {
            userModel.find((error, data) => {
                if(error) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: error.message
                    })
                } 
                    return response.status(200).json({
                        status: "Success: Get All users success",
                        data: data
                    })
                
            })
        }
}

//function lấy user đầu tiên tính từ vị trí skip sau khi được sort alphabet
const sortSkipLimitUser = (request, response) => {
    const skipNumber = request.query.skipNumber;
        
            userModel.find()
                    .sort({fullName:"asc"})
                    .skip(skipNumber)
                    .limit(1) //trả limit phần tử đầu tiên tính từ vị trí skip
                    .exec((error, data)  => {
                        if(error) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: error.message
                            })
                        } 
                        else {
                            return response.status(200).json({
                                status: "Success: Get skip limit users success",
                                data: data
                            })
                        }
                    })
            
       
}
module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllUserLimit,
    getAllUserSkip,
    sortUserByABC,
    getAllUserSkipLimit,
    sortSkipLimitUser
}
