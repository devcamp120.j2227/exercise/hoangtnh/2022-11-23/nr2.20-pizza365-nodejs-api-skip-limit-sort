// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user controller
const userController = require("../controllers/userController")

router.post("/users", userController.createUser);
router.get("/users", userController.getAllUsers);
router.get("/users/:userId", userController.getUserById);
router.put("/users/:userId", userController.updateUserById);
router.delete("/users/:userId", userController.deleteUserById);
router.get("/limit-users", userController.getAllUserLimit);
router.get("/skip-users", userController.getAllUserSkip);
router.get("/sort-users", userController.sortUserByABC);
router.get("/skip-limit-users",userController.getAllUserSkipLimit)
router.get("/sort-skip-limit-users",userController.sortSkipLimitUser)
module.exports = router;